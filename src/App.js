import React, { useState, useEffect, createContext } from "react";
import ScrollToTop from "react-scroll-to-top";
import "./App.css";
import Navbar from "./components/Navbar/Navbar";
import Home from "./components/Home/Home";
import About from "./components/About/About";
import Contact from "./components/Contact/Contact";
import Projects from "./components/Projects/Projects";
import Footer from "./components/Footer/Footer";
import Skills from "./components/Skills/Skills";
import ChatBot from "react-chatbotify";

export const ThemeContext = createContext(null);

function App() {
  const [theme, setTheme] = useState("light");
  const helpOptions = [
    "Frontend",
    "Backend",
    "Base de données",
    "Intégration Continue",
    "Architecture",
    "DevOps",
  ];
  const flow = {
    start: {
      message:
        "👋! Bienvenue, Je suis heureux de pouvoir partager mes compétences avec vous 😊",
      transition: { duration: 1000 },
      path: "show_options",
    },
    show_options: {
      message:
        
        "Choisissez une option pour découvrir mes compétences actives",
      options: helpOptions,
      path: "process_options",
    },
    prompt_again: {
      message: "Avez-vous un autre choix ?",
      options: helpOptions,
      path: "process_options",
    },
    unknown_input: {
      message:
        "Je m'excuse, je ne comprends pas votre message 😢! ",
      options: helpOptions,
      path: "process_options",
    },
    process_options: {
      transition: { duration: 0 },
      chatDisabled: true,
      path: async (params) => {
        switch (params.userInput) {
          case "Frontend":
            await params.injectMessage("ReactJS, Angular JS");

            break;
          case "Backend":
            await params.injectMessage(
              "ExpressJS, Spring Boot, NestJS , Flask"
            );

            break;
          case "Base de données":
            await params.injectMessage("MySQL, MongoDB, Postgres");

            break;
        
          case "Intégration Continue":
            await params.injectMessage("Git, Github, Gitlab");
            break;
          case "Architecture":
            await params.injectMessage("MVC, Microservices");
            break;
          case "DevOps":
            await params.injectMessage("Jenkins, Docker, Sonarqube, Kubernetes, Junit, Chai, Ubuntu");
            break;
          default:
            return "unknown_input";
        }

        /* 				setTimeout(() => {
					window.open(link);
				}, 1000) */
        return "repeat";
      },
    },
    repeat: {
      transition: { duration: 1000 },
      path: "prompt_again",
    },
  };

  const body = document.getElementsByTagName("BODY")[0];

  const changeTheme = () => {
    setTheme((curr) => (curr === "light" ? "dark" : "light"));
  };

  useEffect(() => {
    if (theme === "dark") {
      body.style.background = "#000";
      body.style.color = "#dddddd";
    } else {
      body.style.background = "#ffffff";
      body.style.color = "#656d72";
    }
  }, [body.style, theme]);

  return (
    <ThemeContext.Provider value={{ theme, changeTheme }}>
      <div className="App" id={theme}>
        <Navbar />
        <Home />
        <About />
        <Skills />
        {/* <Resume /> */}
        <Projects />
        <Contact />
        <div align="justify">
          <ScrollToTop smooth />
        </div>
        <ChatBot
          settings={{
            general: { embedded: false },
            chatHistory: { storageKey: "example_faq_bot" },
          }}
          flow={flow}
        />
        <Footer />
      </div>
    </ThemeContext.Provider>
  );
}

export default App;
