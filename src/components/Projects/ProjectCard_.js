import React, { useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { BsLink } from "react-icons/bs";
import styles from "./ProjectCard.module.css";

const ProjectCard_ = () => {
  // eslint-disable-next-line no-unused-vars
  const [sliderRef, setSliderRef] = useState(null);

  const sliderSettings = {
    arrows: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  const projects = [
    {
      img: "/projects/courzelo.png",
      description:"Intégration d’un service de paiement et un système de recommandation des cours les plus achetés dans une solution de e-learning",
      name: "CourZelo.  ",
      tech: ["Angular","Java","TypeScript", "Spring Boot", "MongoDB"],
      link: "https://www.courzelo.com/",
    },
    {
      img: "/projects/cliclaim.png",
      name:"CliClaim.",
      description: "Intégration d’un service de paiement dans une solution de gestion des réclamations ",
      tech: ["React", "NodeJS", "Redux","Nest JS","MongoDB"],
      link: "http://194.146.13.225:30466",
    },

   
  ];

  return (
    <div className={styles.content}>
     {/*  <div className={styles.controls}>
        <button onClick={sliderRef?.slickPrev}>
          <FaChevronLeft />
        </button>
        <button onClick={sliderRef?.slickNext}>
          <FaChevronRight />
        </button>
      </div> */}
      <Slider ref={setSliderRef} {...sliderSettings}>
        {projects.map((card, index) => (
          <div key={index} className={styles.card}>
            <img
              src={process.env.PUBLIC_URL + card.img}
              alt={card.name}
              className={styles.card_img}
            />
            <div className={styles.text_info}>
              <div className={styles.card_header}>
                <h2>{card.name}</h2>
              </div>
              <p style={{fontSize:10, textAlign:"justify",lineHeight:2}}>{card.description}</p>

              <div className={styles.techContainer}>
                {card.tech.map((item,index_) => (
                  <div key={index_} className={styles.tech}>{item}</div>
                ))}
              </div>
              <a className={styles.link} href={card.link} target="_blank" rel="noreferrer">
                <BsLink />
              </a>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default ProjectCard_;
