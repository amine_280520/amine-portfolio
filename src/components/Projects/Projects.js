/* eslint-disable react/jsx-pascal-case */
import React from "react";
import ProjectCard from "./ProjectCard";
import styles from "./Projects.module.css";
import ProjectCard_ from "./ProjectCard_";

const Projects = () => {
  return (
    <React.Fragment>
      <section style={{marginTop : 0}} className={""} id="work">
        <div className={styles.heading}>
          <h2 className={styles["title"]}>Projets</h2>
        </div>
        <div className={styles.container}>
          <h2 style={{ textAlign: "justify" }} className={styles["title"]}>
            <span
              className="mx-5"
              style={{ border: "2px solid #91A8D8" }}
            ></span>
            Dont j'ai réalisé
          </h2>
          <br/>
          <ProjectCard />
        </div>
        <div style={{ marginTop: 50 }} className={styles.container}>
          <h2 style={{ textAlign: "justify" }} className={styles["title"]}>
            <span
              className="mx-5"
              style={{ border: "2px solid #91A8D8" }}
            ></span>
            Dont j'ai participé
          </h2>
          <br/>
          <ProjectCard_ />
        </div>
      </section>
    </React.Fragment>
  );
};

export default Projects;
