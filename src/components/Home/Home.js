import React from "react";
import styles from "./Home.module.css";
import Banner from "../../assets/bg2.png";
import SocialIcons from "./SocialIcons";

const Home = () => {
  return (
    <div>
      <section className={styles.home_section} id="home">
        <div className={styles.home_left}>
          <div className={styles.justifyContent}>
            <p className={styles.greet}>Bonjour, je m'appelle</p>
          </div>

          <div className={styles.justifyContent}>
            <h1 className={styles.heading_1}>
              Med Amine <span style={{ color: "#85a1c1" }}>LARIBI.</span>
            </h1>
          </div>
          {/* <h1 className={styles.heading_2}>I build things for the web.</h1> */}
          <div className={styles.justifyContent}>
            <p className={styles.desc}>
              Développeur FullStack, créant des solutions web performantes et
              sur mesure. Passionné, je transforme vos idées en projets digitaux
              prêts à relever les défis actuels.
            </p>
          </div>
        </div>
        <div className={styles.home_right}>
          <div className={styles.home_img}>
            <img
              style={{
                marginTop: -50,
                marginLeft: -50,
                height: 300,
                borderRadius: "5px",
                cursor: "auto",
              }}
              alt=""
              className={styles.image}
              src={Banner}
            />
          </div>
        </div>
      </section>
      <SocialIcons />
      <br />
    </div>
  );
};

export default Home;
