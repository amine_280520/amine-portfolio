import React from 'react';
import styles from "./Home.module.css";
import { FiGithub, FiGitlab } from "react-icons/fi";
import { FiLinkedin } from "react-icons/fi";
function SocialIcons() {
    return (
        <div className={styles.social_icons}>
        <a
          href="https://github.com/MedAmine22?tab=repositories"
          target="_blank"
          rel="noreferrer"
        >
          <FiGithub />
        </a>
        <a
          href="https://www.linkedin.com/in/mohamed-amine-laribi/"
          rel="noreferrer"
          target="_blank"
        >
          <FiLinkedin />
        </a>
      </div>
    );
}

export default SocialIcons;