import React, { useRef } from "react";
import {ToastContainer, toast } from "react-toast";
import styles from "./ContactForm.module.css";
import _ from 'emailjs-com';


function ContactForm() {
  
  const form = useRef();

  const sendMail = (e) => {
    e.preventDefault();
      _.sendForm(
          'service_abxnirb','template_q3wza0a',form.current,'fxTKjBwi7g5_Fblhn'
        ).then(()=>{
        toast.success("Email Sended Successfuly.");
      }).catch(()=>{
        toast.error("Oops. Something Went Wrong !!");
      });
  }

  return (
    <div className={styles["contact-form"]}>
      <form ref={form}  name="contactForm">
        
        <div className={styles["form-control"]}>
          <input
            id="user_name"
            name="user_name"
            type="text"
            placeholder="Name"
            required
          />
        </div>
        <div className={styles["form-control"]}>
          <input
            id="subject"
            name="subject"
            type="text"
            placeholder="Subject"
            required
          />
        </div>
        <div className={styles["form-control"]}>
          <input
            id="user_email"
            name="user_email"
            type="text"
            placeholder="Email"
            required
          />
        </div>
        <div className={styles["form-control"]}>
          <textarea
            id="message"
            name="message"
            rows="8"
            placeholder="Message"
            required
          ></textarea>
        </div>
        <button onClick={sendMail} className={styles["submit-btn"]}>Submit</button>
      </form>
      <ToastContainer delay={3000} />


    </div>
  );
}

export default ContactForm;
