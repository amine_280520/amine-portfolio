import React from "react";
import ContactInfo from "./ContactInfo";
import styles from "./Contact.module.css";
import ContactForm from './ContactForm';


function Contact() {
  return (
    <section style={{backgroundColor:'#f9f7f7', marginTop: 50}} className={styles.contact_section} id="contact">
      <div className={styles.heading}>
        <h2 className={styles["title"]}>Contact</h2>
      </div>
      <div className={styles.container}>
       
       
        <div className={styles.left}>
          <ContactForm/>
        </div>
        <div className={styles.right}>
          <ContactInfo />
        </div>
      </div>
      <div style={{paddingTop: 90}}></div>
    </section>
  );
}

export default Contact;
