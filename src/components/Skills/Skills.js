import React from "react";
import styles from "./Skills.module.css";
import styles_ from "./Flap.module.css";

const Skills = () => {
  return (
    <React.Fragment>
      <section style={{backgroundColor:'#f9f7f7'}} className={styles.skills} id="skills">
        <div className={styles.heading}>
          <h2 className={styles["title"]}>Compétences Techniques</h2>
        </div>

        <div style={{marginLeft: 42, padding : 10}} className={styles["skills_container"]}>
          <div style={{marginTop : 90}} className={styles["skills_wrapper"]}>
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <div className="row">
                  <div className="col-12 d-flex justify-content-center align-items-center">
                    <img
                      style={{ marginTop: 25, marginLeft: 51 }}
                      height={100}
                      width={100}
                      src={"https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png"}
                      alt=""
                      title="React JS"
                    />
                  </div>
                </div>
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <div className="row">
                  <div className="col-12 d-flex justify-content-center align-items-center">
                    <img
                      style={{ marginTop: 25, marginLeft: 25 }}
                      height={100}
                      width={150}
                      src={"https://miro.medium.com/v2/resize:fit:1200/1*8vDTQKhEmn_2_TVZ-u8mQw.jpeg"}
                      alt=""
                      title="Redux"
                    />
                  </div>
                </div>
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <div className="row">
                  <div className="col-12 d-flex justify-content-center align-items-center">
                    <img
                      style={{ marginTop: 25, marginLeft: 25 }}
                      height={100}
                      width={150}
                      src={"https://inapp.com/wp-content/uploads/elementor/thumbs/express-js-01-1-q05uw85vt1jqloiy5k82sfy7tgvysgt1uqld8slsbc.png"}
                      alt=""
                      title="Express JS"
                    />
                  </div>
                </div>
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <div className="row">
                  <div className="col-12 d-flex justify-content-center align-items-center">
                    <img
                      style={{ marginTop: 25, marginLeft: 25 }}
                      height={100}
                      width={150}
                      src={"https://cdn.icon-icons.com/icons2/2699/PNG/512/nestjs_logo_icon_169927.png"}
                      alt=""
                      title="Nest JS"
                    />
                  </div>
                </div>
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <div className="row">
                  <div className="col-12 d-flex justify-content-center align-items-center">
                    <img
                      style={{ marginTop: 25, marginLeft: 25 }}
                      height={100}
                      width={150}
                      src={"https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Flask_logo.svg/1280px-Flask_logo.svg.png"}
                      alt=""
                      title="Flask"
                    />
                  </div>
                </div>
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <div className="row">
                  <div className="col-12 d-flex justify-content-center align-items-center">
                    <img
                      style={{ marginTop: 45, marginLeft: 25 }}
                      height={"auto"}
                      width={150}
                      src={"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/2560px-Node.js_logo.svg.png"}
                      alt=""
                      title="Node JS"
                    />
                  </div>
                </div>
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <img
                  style={{ marginTop: 5, marginLeft : 35}}
                  height={"auto"}
                  width={140}
                  src={"https://i0.wp.com/code4developers.com/wp-content/uploads/2017/06/angular.png?fit=500%2C500&ssl=1"}
                  alt=""
                  title="Angular JS"
                />
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <img
                  style={{ marginTop: 45, marginLeft: 30 }}
                  height={55}
                  width={140}
                  src={"https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Spring_Framework_Logo_2018.svg/1200px-Spring_Framework_Logo_2018.svg.png"}
                  alt=""
                  title="Spring"
                />
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <img
                  style={{ marginTop: 45, marginLeft: 10 }}
                  height={"auto"}
                  width={180}
                  src={"https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/MongoDB_Logo.svg/2560px-MongoDB_Logo.svg.png"}
                  alt=""
                  title="Mongo DB - NOSQL"
                />
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <img
                  style={{ marginTop: 25, marginLeft: 25 }}
                  height={"auto"}
                  width={150}
                  src={process.env.PUBLIC_URL + "/logos/ms.png"}
                  alt=""
                  title="MicroService Architecture"
                />
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <img
                  style={{ marginTop: 45, marginLeft: 35 }}
                  height={60}
                  width={130}
                  src={"https://git-scm.com/images/logos/downloads/Git-Logo-2Color.png"}
                  alt=""
                  title="Git (Version Control System)"
                />
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <img
                  style={{ marginTop: 25, marginLeft: 55 }}
                  height={90}
                  width={100}
                  src={"https://seeklogo.com/images/D/docker-logo-6D6F987702-seeklogo.com.png"}
                  alt=""
                  title="Docker"
                />
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <img
                  style={{ marginTop: 25, marginLeft: 15 }}
                  height={100}
                  width={180}
                  src={"https://cdn.worldvectorlogo.com/logos/sonarqube.svg"}
                  alt=""
                  title="Sonarqube"
                />
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <img
                  style={{ marginTop: 25, marginLeft: 50 }}
                  height={100}
                  width={"auto"}
                  src={"https://avatars.githubusercontent.com/u/1515293?v=4"}
                  alt=""
                  title="Chai"
                />
              </div>

              <div className={styles_.flap}></div>
            </div>
            
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <img
                  style={{ marginTop: 25, marginLeft: 25 }}
                  height={90}
                  width={"auto"}
                  src={"https://1000logos.net/wp-content/uploads/2022/07/Kubernetes-Logo.png"}
                  alt=""
                  title="Kubernetes"
                />
              </div>

              <div className={styles_.flap}></div>
            </div>
            &nbsp;
            <div style={{marginTop : 10}} className={styles_.container}>
              <div align="center" className={styles_.content}>
                <img
                  style={{ marginTop: 25, marginLeft: 5 }}
                  height={100}
                  width={200}
                  src={"https://logos-world.net/wp-content/uploads/2023/12/Jenkins-Emblem.png"}
                  alt=""
                  title="Jenkins"
                />
              </div>

              <div className={styles_.flap}></div>
            </div>
           
          </div>
          
          
          &nbsp; &nbsp;
        </div>
      </section>
    </React.Fragment>
  );
};

export default Skills;
