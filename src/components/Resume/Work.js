import React from "react";
import styles from "./Work.module.css";
import { BsFillBriefcaseFill } from "react-icons/bs";

const Work = () => {
  return (
    <div className={styles.container}>
      <div className={styles.timeline_wrapper}>
        <div>
          <div className={styles.timeline_icon}>
            <BsFillBriefcaseFill />
          </div>
          <div className={styles.timeline_start}></div>
          {/* <h3>Work</h3> */}
        </div>
        <div className={styles.timeline}>
        <div className={styles.timeline_item}>
            <h4 className={styles.timeline_title}>
              Training for employees in BNA
            </h4>
            <p className={styles.timeline_date}>2024 - BNA -  MOHAMED V - Tunis</p>
            <p className={styles.timeline_desc}>
              <span style={{ fontWeight: "bold", fontSize: 14 }}>
                Key Words
              </span>{" "}
              : &nbsp;
              <span style={{ fontSize: 14 }}>
                React APP, Introdction , Hooks, States, Styles, DOM , DOM Manipulation, Javascript, Build Own App using ReactJS 
              </span>
            </p>
          </div>
          <div className={styles.timeline_item}>
            <h4 className={styles.timeline_title}>
              Training for instructors in DevOps
            </h4>
            <p className={styles.timeline_date}>2023 - ESPRIT GHAZELA</p>
            <p className={styles.timeline_desc}>
              <span style={{ fontWeight: "bold", fontSize: 14 }}>
                Key Words
              </span>{" "}
              : &nbsp;
              <span style={{ fontSize: 14 }}>
                Setting up a Jenkins pipeline that encompasses the deployment
                process from build creation to deployment.
              </span>
            </p>
          </div>
          <div className={styles.timeline_item}>
            <h4 className={styles.timeline_title}>Supervision of students</h4>
            <p className={styles.timeline_date}>
              2022 - IPACT Consult inc Canada
            </p>
            <p className={styles.timeline_desc}>
              <span style={{ fontWeight: "bold", fontSize: 14 }}>
                Key Words
              </span>{" "}
              : &nbsp;
              <span style={{ fontSize: 14 }}>
                Supervision, Assistance, Quality, Delivery
              </span>
            </p>
          </div>
          <div className={styles.timeline_item}>
            <h4 className={styles.timeline_title}>
              Collaborate with IPACT Consult as a FullStack Developer
            </h4>
            <p className={styles.timeline_date}>
              2022 - IPACT Consult inc Canada
            </p>
            <p className={styles.timeline_desc}>
              <span style={{ fontWeight: "bold", fontSize: 14 }}>
                Key Words
              </span>{" "}
              : &nbsp;
              <span style={{ fontSize: 14 }}>FullStack Developer</span>
            </p>
          </div>
          <div className={styles.timeline_item}>
            <h4 className={styles.timeline_title}>
              Development of a web application for a guest house "Dar Zaghouane"
            </h4>
            <p className={styles.timeline_date}>
              2022 - IPACT Consult inc Canada
            </p>
            <p className={styles.timeline_desc}>
              <span style={{ fontWeight: "bold", fontSize: 14 }}>
                Key Words
              </span>{" "}
              : &nbsp;
              <span style={{ fontSize: 14 }}>
                ReactJS, NodeJS, ExpressJS, SQL ,PHPMYADMIN , OVH Server
              </span>
            </p>
          </div>
          <div className={styles.timeline_item}>
            <h4 className={styles.timeline_title}>
              Design & Implement a Human Resources Module in an ERP
            </h4>
            <p className={styles.timeline_date}>
              2022 - IPACT Consult inc Canada
            </p>
            <p className={styles.timeline_desc}>
              <span style={{ fontWeight: "bold", fontSize: 14 }}>
                Key Words
              </span>{" "}
              : &nbsp;
              <span style={{ fontSize: 14 }}>
                Angular, Spring boot, Micro services, Docker, Déploiement,
                Sonar, Jenkins,Scss, MongoDB, SEO, BI, Scrum, Scrum master,
                Ubuntu, Oxahost
              </span>
            </p>
          </div>
          <div className={styles.timeline_item}>
            <h4 className={styles.timeline_title}>
              Design & Implement a Consulting Website
            </h4>
            <p className={styles.timeline_date}>
              2021 - IPACT Consult inc Canada
            </p>
            <p className={styles.timeline_desc}>
              <span style={{ fontWeight: "bold", fontSize: 14 }}>
                Key Words
              </span>{" "}
              : &nbsp;
              <span style={{ fontSize: 14 }}>
                React, NodeJs, ExpressJs, MongoDB, Deployment, Sonar, SEO, SEM
              </span>
            </p>
          </div>
          <div className={styles.timeline_item}>
            <h4 className={styles.timeline_title}>
              Design & Implement a Mediterranean Website
            </h4>
            <p className={styles.timeline_date}>
              2021 - IPACT Consult inc Canada
            </p>
            <p className={styles.timeline_desc}>
              <span style={{ fontWeight: "bold", fontSize: 14 }}>
                Key Words
              </span>{" "}
              : &nbsp;
              <span style={{ fontSize: 14 }}>
                React, NodeJs, ExpressJs, MongoDB, SEO, SEM
              </span>
            </p>
          </div>
          <div className={styles.timeline_item}>
            <h4 className={styles.timeline_title}>
              Design & Implement a Generic Web Application
            </h4>
            <p className={styles.timeline_date}>
              2021 - Minos Search Group, Tunis
            </p>
            <p className={styles.timeline_desc}>
              <span style={{ fontWeight: "bold", fontSize: 14 }}>
                Key Words
              </span>{" "}
              : &nbsp;
              <span style={{ fontSize: 14 }}>
                React, NodeJs, ExpressJs, MongoDB
              </span>
            </p>
          </div>
          <div className={styles.timeline_item}>
            <h4 className={styles.timeline_title}>
              Design & Implement a Web, Mobile & Desktop E-Commerce Platform
            </h4>
            <p className={styles.timeline_date}>
              2020 - ESPRIT, Ghazela, Tunis
            </p>
            <p className={styles.timeline_desc}>
              <span style={{ fontWeight: "bold", fontSize: 14 }}>
                Key Words
              </span>{" "}
              : &nbsp;
              <span style={{ fontSize: 14 }}>
                Symfony, JavaFX, CodenameOne, MySQL, Java, PHP, SQL, TWIG
              </span>
            </p>
          </div>
          <div className={styles.timeline_item}>
            <h4 className={styles.timeline_title}>
              Design & Implement a Web & Mobile Application for Food Delivery
            </h4>
            <p className={styles.timeline_date}>
              2019 - Medianet Consulting, Tunis
            </p>
            <p className={styles.timeline_desc}>
              <span style={{ fontWeight: "bold", fontSize: 14 }}>
                Key Words
              </span>{" "}
              : &nbsp;
              <span style={{ fontSize: 14 }}>
                Symfony, Android Studio, MYSQL, PHP, TWIG, JAVA, SQL
              </span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Work;
