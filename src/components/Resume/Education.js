import React from "react";
import styles from "./Education.module.css";
import { FaGraduationCap } from "react-icons/fa";
import { BsLink } from "react-icons/bs";

const Education = () => {
  return (
    <section id="experience">
      <div className={styles.container}>
        <div className={styles.timeline_wrapper}>
          <div className={styles.timeline_icon}>
            <FaGraduationCap />
          </div>
          
          <div className={styles.timeline}>
          <div className={styles.timeline_item}>
              <h4 className={styles.timeline_title}>Software Engineer</h4>
              <p className={styles.timeline_date}>2019 - 2022</p>
              <p className={styles.timeline_desc}>
                Private Higher School of Engineering and Technology, Tunis
                Ariana, Tunisia
              </p>
              <a className={styles.link} href={"#link"} target="_blank" rel="noreferrer">
                <BsLink />
              </a>
            </div>
            <div className={styles.timeline_item}>
              <h4 className={styles.timeline_title}>
                The fundamentals of Agility & Scrum
              </h4>
              <p className={styles.timeline_date}>2021</p>
              <p className={styles.timeline_desc}>Udemy</p>    
              <a className={styles.link} href={"#link"} target="_blank" rel="noreferrer">
                <BsLink />
              </a>
            </div>
            <div className={styles.timeline_item}>
              <h4 className={styles.timeline_title}>
                Interculturality: Education, Application and Universal Rights
              </h4>
              <p className={styles.timeline_date}>2021</p>
              <p className={styles.timeline_desc}>
                Anna Lindh Foundation & Mediterravenir , Tunis
              </p>
              <a className={styles.link} href={"#link"} target="_blank" rel="noreferrer">
                <BsLink />
              </a>
            </div>
            
            <div className={styles.timeline_item}>
              <h4 className={styles.timeline_title}>
                Certification in Advanced Mobile Application Development
              </h4>
              <p className={styles.timeline_date}>2019</p>
              <p className={styles.timeline_desc}>Bright Technology, Tunis</p>
              <a className={styles.link} href={"#link"} target="_blank" rel="noreferrer">
                <BsLink />
              </a>
            </div>
            <div className={styles.timeline_item}>
              <h4 className={styles.timeline_title}>
                Certification in Mobile Application Development
              </h4>
              <p className={styles.timeline_date}>2018</p>
              <p className={styles.timeline_desc}>Tek-up Ghazela Ariana, Tunis</p>
              <a className={styles.link} href={"#link"} target="_blank" rel="noreferrer">
                <BsLink />
              </a>
            </div>
            <div className={styles.timeline_item}>
              <h4 className={styles.timeline_title}>
              License in Information Systems Development
              </h4>
              <p className={styles.timeline_date}>2015-2019</p>
              <p className={styles.timeline_desc}>Higher School of Technological Studies of Charguia, Ariana, Tunis</p>
              <a className={styles.link} href={"#link"} target="_blank" rel="noreferrer">
                <BsLink />
              </a>
            </div>
            <div className={styles.timeline_item}>
              <h4 className={styles.timeline_title}>
                Bachelor of Experimental Science
              </h4>
              <p className={styles.timeline_date}>2012-2015</p>
              <p className={styles.timeline_desc}>Mahmoud Messaidi School, Bardo, Tunis</p>
              <a className={styles.link} href={"#link"} target="_blank" rel="noreferrer">
                <BsLink />
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Education;
