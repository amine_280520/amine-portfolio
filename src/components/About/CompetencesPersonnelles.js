import React from "react";
import styles from "./Interests.module.css";

const CompetencesPersonnelles = () => {
  return (
    <div className={styles["skills"]}>
      {/* <h1>My current skills</h1> */}
      <div className={styles["skills_container"]}>
        <div className={styles["skills_wrapper"]}>
          <div style={{backgroundColor:'#7090cb',color:'white'}} className={styles.skill}>Communication</div>
          <div style={{backgroundColor:'#7090cb',color:'white'}} className={styles.skill}>Leadership</div>
          <div style={{backgroundColor:'#7090cb',color:'white'}} className={styles.skill}>Organisation</div>
        </div>
      </div>
    </div>
  );
};

export default CompetencesPersonnelles;
