import React from "react";
import styles from "./InterestCard.module.css";

const InterestCard = (props) => {
  return (
    <li className={styles["skillCard"]}>
      <div className={styles["card"]}>
        <div className={styles["card_image"]} title={props.text}>
          <img src={props.src} alt={"-img"}/>
        </div>
      </div>
    </li>
  );
};

export default InterestCard;
