import React from "react";
import styles from "./About.module.css";
import Interests from "./Interests";
import Me from '../../assets/myself.JPG';
import Languages from "./Languages";
import CompetencesPersonnelles from "./CompetencesPersonnelles";

const About = () => {
  return (
    <React.Fragment>
      <section className={styles.about_section} id="about">
        <div className={styles.heading}>
          <h2 className={styles["title"]}>En savoir de moi</h2>
        </div>
        <div className={styles.container}>
          <div className={styles.left}>
            <h3>Faites ma connaissance !</h3>
            <p className={styles.desc}>
              Développeur FullStack basé à Tunis, avec plus d’un an d’expérience
              dans la création de sites web performants et responsives.
              Passionné et engagé, je transforme vos idées en solutions
              digitales sur mesure, prêtes à répondre aux défis du monde
              moderne.
            </p>
            <div className={styles.left}>
            <h3>Compétences Personnelles</h3>
            <CompetencesPersonnelles />
          </div>
          </div>
          <div className="d-flex justify-content-center">
            <img
              className="img-responsive"
              style={{
                border: "1px solid #7090cb",
                borderTopLeftRadius: "25px",
                borderBottomRightRadius: "25px",
              }}
              height={"400px"}
              width="auto"
              src={Me}
              alt=""
            />

          </div>

          <div className={styles.left}>
            <h3>Centres d'intérêts</h3>
            <Interests />
          </div>
          <div className={styles.right}>
            <h3>Compétences linguistiques</h3>
            <Languages />
          </div>
        </div>
      </section>
    </React.Fragment>
  );
};

export default About;
