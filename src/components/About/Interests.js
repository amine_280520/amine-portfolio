import React from "react";
import styles from "./Interests.module.css";

const Interests = () => {
  return (
    <div className={styles["skills"]}>
      {/* <h1>My current skills</h1> */}
      <div className={styles["skills_container"]}>
        <div className={styles["skills_wrapper"]}>
          <div style={{backgroundColor:'#7090cb',color:'white'}} className={styles.skill}>Films</div>
          <div style={{backgroundColor:'#7090cb',color:'white'}} className={styles.skill}>Fitness</div>
          <div style={{backgroundColor:'#7090cb',color:'white'}} className={styles.skill}>Ecouter de musqiue</div>
          <div style={{backgroundColor:'#7090cb',color:'white'}} className={styles.skill}>Jeux de foot</div>
        </div>
      </div>
    </div>
  );
};

export default Interests;
