import React from "react";
import styles from "./Interests.module.css";

import TN from '../../assets/tn.png';
import FR from '../../assets/fr.png';
import ENG from '../../assets/eng.PNG';

const Languages = () => {
  return (
    <div style={{marginTop:-15}} className={styles["skills"]}>
      {/* <h1>My current skills</h1> */}
      <div style={{marginLeft:-15}} className={styles["skills_container"]}>
        <div className={styles["skills_wrapper"]}>
        <div  className={styles.skill}>
                <img style={{borderRadius:5}} src={ENG} width={70} height={50} alt=""/>
          </div>
        <div  className={styles.skill}>
                <img style={{borderRadius:5}} src={FR} width={70} height={50} alt=""/>
          </div>
      
          <div  className={styles.skill}>
                <img style={{borderRadius:5}} src={TN} width={70} height={50} alt=""/>
          </div>

        </div>
      </div>
    </div>
  );
};

export default Languages;
