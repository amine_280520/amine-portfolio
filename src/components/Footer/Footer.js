import React from "react";
import {  FiGithub, FiGitlab } from "react-icons/fi";
import { FiLinkedin } from "react-icons/fi";
import styles from "./Footer.module.css";

function Footer() {
  const year = () => {
    const d = new Date();
    let y = d.getFullYear();
    return y;
  };

  return (
    <div className={styles.footer}>
      <div>
        <a
          className={styles.socialIcon}
          href="https://github.com/MedAmine22?tab=repositories"
          target="_blank" rel="noreferrer"
        >
          <FiGithub />
        </a>
        <a
          className={styles.socialIcon}
          href="#linkedin"
          target="_blank" rel="noreferrer"
        >
          <FiLinkedin />
        </a>
       
      </div>

      <p className="copyright">
        © <span className="year">{year()}</span> -{" "}
        <span className="bold">Med Amine LARIBI.</span>
      </p>
    </div>
  );
}

export default Footer;
